//
//  VideoPlayer.m
//  Pods
//
//  Created by Alex on 01/06/2017.
//
//

#import <Foundation/Foundation.h>
#import "VideoPlayer.h"
@import AVFoundation;
@import AVKit;

@interface VideoPlayer ()

@property(nonatomic, readwrite) AVPlayer *player;
@property(nonatomic, readwrite) UILabel *videoInfo;
@property(nonatomic, readwrite) UILabel *videoDuration;
@property(nonatomic, readwrite) UILabel *videoTime;
@property(nonatomic, readwrite) UIButton *playButton;
@property(nonatomic, readwrite) UISlider *seekBar;

@property(nonatomic, readwrite) CGFloat height;
@property(nonatomic, readwrite) CGFloat width;

@property(nonatomic, readwrite) Boolean playerStatus;
@property(nonatomic, readwrite) Boolean screenToogle;


@end

@implementation VideoPlayer : UIView

#pragma Implementation

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        _playerStatus = NO;
        _screenToogle = YES;
    }
    
    return self;
}


#pragma Implementation Class methods

+ (void)showVideoPlayerIn:(UIView *)view withVideo:(NSURL *)urlVideo{
    
    CGFloat width = view.frame.size.width;
    CGFloat height = view.frame.size.height;
    AVPlayer *player = [AVPlayer playerWithURL:urlVideo];
    AVPlayerLayer *playerLayer = [AVPlayerLayer layer];
    playerLayer.player = player;
    
    playerLayer.frame = CGRectMake(0, 0, width, height);
    playerLayer.backgroundColor = [UIColor blackColor].CGColor;
    playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    
    [view.layer addSublayer:playerLayer];
    
    [player play];
}

#pragma Implementation Instance methods

- (void)showVideoPlayerIn:(UIView *)view withVideo:(NSURL *)urlVideo withColor:(UIColor *)color withEpicGravityType:(NSInteger) epicLayerGravityType{
    
    _width = view.frame.size.width;
    _height = view.frame.size.height;
    
    [self videoPlayerViewConfiguration:view withVideoUrl:urlVideo withEpicGravityType: epicLayerGravityType];
    
    [self viewConfiguration:view withVideoUrl:urlVideo withColor:color];
    
    [self viewLogicConfiguration:view];
    
}

#pragma VideoPlayerViewConfiguration

- (void)videoPlayerViewConfiguration:(UIView *)view withVideoUrl:(NSURL *)urlVideo  withEpicGravityType:(NSInteger) epicLayerGravityType{

    _player = [AVPlayer playerWithURL:urlVideo];
    AVPlayerLayer *playerLayer = [AVPlayerLayer layer];
    playerLayer.player = _player;
    
    playerLayer.frame = CGRectMake(0, 0, _width, _height);
    playerLayer.backgroundColor = [UIColor blackColor].CGColor;
    

    switch (epicLayerGravityType) {
        case EpicPlayerLayerResize:
            playerLayer.videoGravity = AVLayerVideoGravityResize;
            break;
        case EpicPlayerLayerResizeAspect:
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
            break;
        case EpicPlayerLayerResizeAspectFill:
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            break;
        default:
            playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            break;
    }
    
    [view.layer addSublayer:playerLayer];

}

- (void)viewConfiguration:(UIView *)view withVideoUrl:(NSURL *)urlVideo withColor:(UIColor *) color{
   
    CGFloat buttonSize = _width/16;
    
    NSString *url = urlVideo.absoluteString;;
    NSArray *parts = [url componentsSeparatedByString:@"/"];
    NSString *filename = [parts lastObject];
    
    int seconds = (int) CMTimeGetSeconds(_player.currentItem.asset.duration) % 60;
    int minutes = (int)(CMTimeGetSeconds(_player.currentItem.asset.duration) / 60) % 60;
    int hours = (int) CMTimeGetSeconds(_player.currentItem.asset.duration) / 3600;
        
    
    _videoInfo = [[UILabel alloc] initWithFrame:CGRectMake(buttonSize/3, buttonSize/3, _width-(buttonSize*2), buttonSize)];
    [_videoInfo setTextColor:[UIColor whiteColor]];
    [_videoInfo setText:filename];
    [_videoInfo setAdjustsFontSizeToFitWidth:YES];
    
    
    _videoDuration = [[UILabel alloc] initWithFrame:CGRectMake(buttonSize/3, 2*(buttonSize/3)+buttonSize,_width-(buttonSize*2), buttonSize)];
    [_videoDuration setTextColor:[UIColor whiteColor]];
    [_videoDuration setAdjustsFontSizeToFitWidth:YES];
    
    _videoTime = [[UILabel alloc] initWithFrame:CGRectMake(buttonSize/3, 2*(buttonSize/3)+buttonSize*2,_width-(buttonSize*2), buttonSize)];
    [_videoTime setTextColor:[UIColor whiteColor]];
    [_videoTime setAdjustsFontSizeToFitWidth:YES];
    
    if (hours==0) {
        [_videoDuration setText:[NSString stringWithFormat:@"%02d:%02d", minutes, seconds]];
    }else{
        [_videoDuration setText:[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]];

    }
    
    _playButton = [[UIButton alloc] initWithFrame:CGRectMake(_width/2-buttonSize, _height/2-buttonSize, buttonSize*2, buttonSize*2)];
    [_playButton setBackgroundColor:color];
    _playButton.transform = CGAffineTransformMakeRotation(M_PI / -4);
    
    _seekBar = [[UISlider alloc] initWithFrame:CGRectMake(0, _height-buttonSize/2, _width, buttonSize/2)];
    [_seekBar setThumbTintColor:color];
    [_seekBar setTintColor:color];
    
    
    [view addSubview:_videoInfo];
    [view addSubview:_videoDuration];
    [view addSubview:_videoTime];
    [view addSubview:_playButton];
    [view addSubview:_seekBar];

}

#pragma ViewLogicConfiguration

- (void)viewLogicConfiguration:(UIView *)view{

    CMTime interval = CMTimeMake(33, 1000);
    CMTime duration = _player.currentItem.asset.duration;
    
    __weak typeof(self) weakSelf = self;
    [_player addPeriodicTimeObserverForInterval:interval queue:nil usingBlock: ^(CMTime time) {
        CMTime endTime = CMTimeConvertScale (duration, weakSelf.player.currentTime.timescale, kCMTimeRoundingMethod_RoundHalfAwayFromZero);
        if (CMTimeCompare(endTime, kCMTimeZero) != 0) {
            
            double normalizedTime = (double) _player.currentTime.value / (double) endTime.value;
            [weakSelf.seekBar setValue:normalizedTime];
            
            int seconds = (int) CMTimeGetSeconds(_player.currentTime) % 60;
            int minutes = (int)(CMTimeGetSeconds(_player.currentTime) / 60) % 60;
            int hours = (int) CMTimeGetSeconds(_player.currentTime) / 3600;
            
            if (hours==0) {
                
                [weakSelf.videoTime setText:[NSString stringWithFormat:@"%02d:%02d", minutes, seconds]];
            }else{
                [weakSelf.videoTime setText:[NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds]];
            }
        }
    }];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(screenTouched)];
    
    [view addGestureRecognizer:singleFingerTap];
    
    [_playButton addTarget:self
                    action:@selector(playVideo)
          forControlEvents:UIControlEventTouchUpInside];
    
    [_seekBar addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];


}

#pragma Events and actions

- (void) playVideo{
    
    if (_playerStatus) {
        _playButton.transform = CGAffineTransformMakeRotation(M_PI / -4);
        [_player pause];
        
    }
    else{
        _playButton.transform = CGAffineTransformMakeRotation(M_PI);
        [_player play];
        [self hideControlls:YES];

    }

    _playerStatus = !_playerStatus;
}



- (void) hideControlls:(Boolean )hide{
    
    [_videoInfo setHidden:hide];
    [_videoDuration setHidden:hide];
    [_videoTime setHidden:hide];
    [_playButton setHidden:hide];
    [_seekBar setHidden:hide];

}

- (void) screenTouched{

    [self hideControlls:![_videoInfo isHidden]];
    
}

- (void)sliderValueChanged:(UISlider *)sender {
    
    CMTime videoLength = _player.currentItem.duration;
    float videoLengthInSeconds = videoLength.value/videoLength.timescale;
    
    [_player seekToTime:CMTimeMakeWithSeconds(videoLengthInSeconds*sender.value, 1)];

}

@end
