//
//  VideoPlayer.h
//  Pods
//
//  Created by Alex on 01/06/2017.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, EpicPlayerLayerGravityType) {
    EpicPlayerLayerResize,
    EpicPlayerLayerResizeAspect,
    EpicPlayerLayerResizeAspectFill
};

@interface VideoPlayer : UIView

+ (void)showVideoPlayerIn:(UIView *)view withVideo:(NSURL *)urlVideo;
- (void)showVideoPlayerIn:(UIView *)view withVideo:(NSURL *)urlVideo withColor:(UIColor *)color withEpicGravityType:(NSInteger) epicLayerGravityType;

@end
