//
//  main.m
//  EpicPlayer
//
//  Created by Alex on 06/01/2017.
//  Copyright (c) 2017 Alex. All rights reserved.
//

@import UIKit;
#import "EpicAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EpicAppDelegate class]));
    }
}
