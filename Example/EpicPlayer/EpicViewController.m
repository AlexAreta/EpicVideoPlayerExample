//
//  EpicViewController.m
//  EpicPlayer
//
//  Created by Alex on 06/01/2017.
//  Copyright (c) 2017 Alex. All rights reserved.
//

#import "EpicViewController.h"
#import <EpicPlayer/VideoPlayer.h>
@import  AVFoundation;

@interface EpicViewController ()

@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (nonatomic, readwrite, strong) VideoPlayer *videoPlayer;

@end

@implementation EpicViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _videoPlayer = [[VideoPlayer alloc] init];
    NSString*thePath=[[NSBundle mainBundle] pathForResource:@"Imagine_Dragons-Thunder" ofType:@"mp4"];
    NSURL*url=[NSURL fileURLWithPath:thePath];
    [_videoPlayer showVideoPlayerIn:_videoView withVideo:url withColor:[UIColor redColor] withEpicGravityType:EpicPlayerLayerResizeAspect];

//    _videoPlayer = [[VideoPlayer alloc] init];
//    NSURL*url=[NSURL URLWithString:@"http://techslides.com/demos/sample-videos/small.mp4"];
//    [VideoPlayer showVideoPlayerIn:_videoView withVideo:url];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
