//
//  EpicAppDelegate.h
//  EpicPlayer
//
//  Created by Alex on 06/01/2017.
//  Copyright (c) 2017 Alex. All rights reserved.
//

@import UIKit;

@interface EpicAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
