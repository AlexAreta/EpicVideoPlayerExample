# EpicPlayer

[![CI Status](http://img.shields.io/travis/Alex/EpicPlayer.svg?style=flat)](https://travis-ci.org/Alex/EpicPlayer)
[![Version](https://img.shields.io/cocoapods/v/EpicPlayer.svg?style=flat)](http://cocoapods.org/pods/EpicPlayer)
[![License](https://img.shields.io/cocoapods/l/EpicPlayer.svg?style=flat)](http://cocoapods.org/pods/EpicPlayer)
[![Platform](https://img.shields.io/cocoapods/p/EpicPlayer.svg?style=flat)](http://cocoapods.org/pods/EpicPlayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

EpicPlayer is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "EpicPlayer"
```

## Author

Alex, 100290774@alumnos.uc3m.es

## License

EpicPlayer is available under the MIT license. See the LICENSE file for more info.
